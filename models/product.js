'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Product.belongsTo(models.User, { foreignKey: 'seller_user' });
      Product.belongsTo(models.Category, {foreignKey: 'categories'});    
    }
  }
  Product.init({
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    quantity: DataTypes.INTEGER,
    status: DataTypes.INTEGER,
    seller_user: DataTypes.INTEGER,
    categories: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Product',
    hooks: {
      beforeUpdate(product) {
        if(product.quantity == 0) {
          product.status = 0;
        } else {
          product.status = 1;
        }
      },
      beforeCreate(product) {
        if(product.quantity == 0) {
          product.status = 0;
        } else {
          product.status = 1;
        }
      }
    }
  });
  return Product;
};
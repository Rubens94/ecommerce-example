const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const { fieldsValidate } = require('../middlewares/fieldsValidate');
const UserController = require('../controllers/usersControllers');

router.post('/', [
    check('email', 'Correo inválido').isEmail(),
    check('email', 'El email es obligatorio').not().isEmpty().trim().escape(),
    check('password', 'El password es obligatorio').not().isEmpty().trim(),
    check('password', 'El password debe de tener más de 6 carácteres').isLength({ min: 6 }),
    fieldsValidate
], UserController.login);

module.exports = router;
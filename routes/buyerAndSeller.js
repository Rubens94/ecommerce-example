const express = require('express');
const router = express.Router();
const BuyerAndSellerController = require('../controllers/buyerAndSellerControllers');

router.get('/buyers',
BuyerAndSellerController.getBuyers);

router.get('/categories',
BuyerAndSellerController.getCategoriesByBuyer);

router.get('/sellers',
BuyerAndSellerController.getSellers);

module.exports = router;
const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const { fieldsValidate } = require('../middlewares/fieldsValidate');
const { jwtVerify } = require('../middlewares/jwtVerify');
const { selfOrAdminInTransaction } = require('../middlewares/selfOrAdminInTransaction');
const TransactionController = require('../controllers/transactionsControllers');
const { isAdminRole } = require('../middlewares/isAdminRole');

router.post('/', [
    jwtVerify,
    check('products', 'El producto no puede estar vacío').not().isEmpty().trim().escape(),
    check('quantity', 'El usuario no puede estar vacío').not().isEmpty().trim().escape(),
    fieldsValidate
], TransactionController.postTransaction);

router.put('/:id', [
    jwtVerify,
    selfOrAdminInTransaction,
    check('quantity', 'El usuario no puede estar vacío').not().isEmpty().trim().escape(),
    fieldsValidate
], TransactionController.updateTransactionById);

router.delete('/:id', [
    jwtVerify,
    selfOrAdminInTransaction,
], TransactionController.deleteTransactionById);

router.get('/', [
    jwtVerify,
], TransactionController.getTransactionsByUser);

router.get('/admin', [
    jwtVerify,
    isAdminRole
], TransactionController.getTransactionsByAdmin);

router.get('/:id', [
    jwtVerify,
], TransactionController.getTransactionsByUserById);

module.exports = router;
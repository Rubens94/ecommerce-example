const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const { fieldsValidate } = require('../middlewares/fieldsValidate');
const { jwtVerify } = require('../middlewares/jwtVerify');
const ProductController = require('../controllers/productsControllers');
const { selfOrAdminInProduct } = require('../middlewares/selfOrAdminInProduct');

router.post('/', [
    jwtVerify,
    check('name', 'El nombre no puede estar vacío').not().isEmpty().trim().escape(),
    check('name', 'El nombre debe ser string').isString().trim().escape(),
    check('description', 'El nombre debe ser string').isString().trim().escape(),
    check('quantity', 'La cantidad no puede estar vacío').not().isEmpty().trim(),
    check('categories', 'La categoría no puede estar vacía').not().isEmpty().trim(),
    fieldsValidate
], ProductController.postProduct);

router.put('/:id', [
    jwtVerify,
    selfOrAdminInProduct,
    check('name', 'El nombre no puede estar vacío').not().isEmpty().trim().escape(),
    check('name', 'El nombre debe ser string').isString().trim().escape(),
    check('description', 'El nombre debe ser string').isString().trim().escape(),
    check('quantity', 'La cantidad no puede estar vacío').not().isEmpty().trim(),
    check('categories', 'La categoría no puede estar vacía').not().isEmpty().trim(),
    fieldsValidate
], ProductController.updateProductById);

router.delete('/:id', [
    jwtVerify,
    selfOrAdminInProduct
], ProductController.deleteProductById);

router.get('/',
ProductController.getProducts);

router.get('/search',
ProductController.getProductByName);

router.get('/:id',
ProductController.getProductById);

module.exports = router;
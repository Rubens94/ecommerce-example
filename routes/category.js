const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const { fieldsValidate } = require('../middlewares/fieldsValidate');
const { jwtVerify } = require('../middlewares/jwtVerify');
const { isAdminRole } = require('../middlewares/isAdminRole');
const CategoryController = require('../controllers/categoriesControllers');

router.post('/', [
    jwtVerify,
    isAdminRole,
    check('name', 'El nombre no puede estar vacío').not().isEmpty().trim().escape(),
    check('name', 'El nombre debe ser string').isString().trim().escape(),
    check('description', 'El nombre debe ser string').isString().trim().escape(),
    fieldsValidate
], CategoryController.postCategory);

router.put('/:id', [
    jwtVerify,
    isAdminRole,
    check('name', 'El nombre no puede estar vacío').not().isEmpty().trim().escape(),
    check('name', 'El nombre debe ser string').isString().trim().escape(),
    check('description', 'El nombre debe ser string').isString().trim().escape(),
    fieldsValidate
], CategoryController.updateCategoryById);

router.delete('/:id', [
    jwtVerify,
    isAdminRole,
], CategoryController.deleteCategoryById);

router.get('/', 
CategoryController.getCategories);

router.get('/search',
CategoryController.getCategoryByName);

router.get('/:id', 
CategoryController.getCategoryById);

module.exports = router;
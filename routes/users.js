const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const { fieldsValidate } = require('../middlewares/fieldsValidate');
const UserController = require('../controllers/usersControllers');
const { jwtVerify } = require('../middlewares/jwtVerify');
const { selfOrAdminInUser } = require('../middlewares/selfOrAdminInUser');

router.post('/', [
    check('name', 'El nombre no puede estar vacío').not().isEmpty().trim().escape(),
    check('name', 'El nombre debe ser string').isString().trim().escape(),
    check('email', 'Correo inválido').isEmail(),
    check('email', 'El email es obligatorio').not().isEmpty().trim().escape(),
    check('password', 'El password es obligatorio').not().isEmpty().trim(),
    check('password', 'El password debe de tener más de 6 carácteres').isLength({ min: 6 }),
    fieldsValidate
], UserController.postUser);

router.get('/', [
    jwtVerify
],UserController.getUsers);

router.get('/:id', [
    jwtVerify
],UserController.getUserById);

router.put('/:id', [
    jwtVerify,
    check('name', 'El nombre no puede estar vacío').not().isEmpty().trim().escape(),
    check('name', 'El nombre debe ser string').isString().trim().escape(),
    check('email', 'Correo inválido').isEmail(),
    check('email', 'El email es obligatorio').not().isEmpty().trim().escape(),
    fieldsValidate,
    selfOrAdminInUser
],UserController.updateUserById);

router.delete('/:id', [
    jwtVerify,
    selfOrAdminInUser
], UserController.deleteUserById);

module.exports = router;
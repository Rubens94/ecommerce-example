const express = require('express');
const cors = require('cors');
require('dotenv').config();
const logger = require('morgan');

const usersRouter = require('./routes/users');
const loginRouter = require('./routes/login');
const categoryRouter = require('./routes/category');
const productRouter = require('./routes/product');
const transactionRouter = require('./routes/transaction');
const buyerAndSellerRouter = require('./routes/buyerAndSeller');

const app = express();

app.use(cors());

app.use(express.json());
app.use( express.urlencoded({extended: true}) );

app.use(logger('dev'));

app.use('/users', usersRouter);
app.use('/login', loginRouter);
app.use('/category', categoryRouter);
app.use('/products', productRouter);
app.use('/transactions', transactionRouter);
app.use('/', buyerAndSellerRouter);

module.exports = app;
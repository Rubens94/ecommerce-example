const Model = require('../models');
const { User } = Model;
const httpCodes = require('../common/httpCodes');
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

class UserController {
    static async postUser(req, res){
        const { name, email, password } = req.body;

        try {
            const user = await User.findOne({ where: { email } });
    
            if( user ) return res.status(httpCodes.BAD_REQUEST).json({msg: 'El correo ya existe, intente con otro.'});
            
            await User.create({
                name,
                email,
                password
            });
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }

        res.status(httpCodes.OK).json({
            msg: 'usuario creado'
        });

    }

    static async login(req, res){
        const { email, password} = req.body;

        const user = await User.findOne({ where: { email } });
        if( !user ) return res.status(httpCodes.NOT_FOUND).json({msg: 'El correo no está registrado.'});

        const validPassword = bcrypt.compareSync( password, user.password );
        if ( !validPassword ) return res.status(httpCodes.BAD_REQUEST).json({msg: 'Password incorrecto'});

        const { id } = user;
        const token = jwt.sign({ id }, process.env.JWT_SECRET, { expiresIn: "4h" });

        res.status(httpCodes.OK).json({token});
    }

    static async getUsers(req, res){
        const users = await User.findAll();

        if(!users.length) return res.status(httpCodes.NOT_FOUND).json({msg: 'No existen usuarios registrados'});

        res.status(httpCodes.OK).json({ users });
    }

    static async getUserById(req, res){
        const { id } = req.params;

        try{
            const user = await User.findOne({ where: { id }});
            if(!user) return res.status(httpCodes.NOT_FOUND).json({msg: `Usuario con ID: ${id}, no existe`});
            res.status(httpCodes.OK).json({user});
        } catch(err){
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }

    }

    static async updateUserById(req, res){
        const { id } = req.params;
        const { name, email, password } = req.body;
        try{

            const user = await User.findOne({ where: { id }});
            if(!user) return res.status(httpCodes.NOT_FOUND).json({msg: `Usuario con ID: ${id}, no existe`});

            if ( password ) {
                const newPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(10) );
                user.update({
                    name,
                    email,
                    password: newPassword
                });
            }

            user.update({
                name,
                email
            });
    
        } catch(err){
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }

        res.status(httpCodes.OK).json({msg:'Usuario actualizado'});
    }

    static async deleteUserById(req, res) {
        const { id } = req.params;

        try{
            const user = await User.findOne({ where: { id }});
            if(!user) return res.status(httpCodes.NOT_FOUND).json({msg: `Usuario con ID: ${id}, no existe`});
            user.destroy();
        } catch(err){
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }

        res.status(httpCodes.OK).json({msg: 'Usuario eliminado'});
    }
}

module.exports = UserController;
const Model = require('../models');
const { Product, User, Category } = Model;
const httpCodes = require('../common/httpCodes');
const jwt = require("jsonwebtoken");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

class ProductController {
    static async postProduct(req, res){
        const { name, description, quantity, categories } = req.body;
        const token = req.headers.authorization.split(' ')[1];
        const { id } = jwt.verify(token, process.env.JWT_SECRET);

        if(quantity < 0) return res.status(httpCodes.FORBIDDEN).json({msg: 'No se puede poner una cantidad negativa'});

        try{
            await Product.create({
                name,
                description,
                quantity,
                seller_user: id,
                categories
            });
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }

        res.status(httpCodes.OK).json({ msg: 'Producto creado' });
    }

    static async updateProductById(req, res){
        const { id } = req.params;
        const { name, description, quantity, categories } = req.body;

        if(quantity < 0) return res.status(httpCodes.FORBIDDEN).json({msg: 'No se puede poner una cantidad negativa'});

        try{
            const product = await Product.findOne({where: { id } });
            if(!product) res.status(httpCodes.NOT_FOUND).json({msg: `El producto con ID: ${id}, no existe`});
    
            product.update({
                name,
                description,
                quantity,
                categories
            });
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
        
        res.status(httpCodes.OK).json({msg:'Producto actualizado'});
    }

    static async deleteProductById(req, res){
        const { id } = req.params;

        try{
            const product = await Product.findOne({where: { id } });
            if(!product) res.status(httpCodes.NOT_FOUND).json({msg: `El producto con ID: ${id}, no existe`});
    
            product.destroy();
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
        
        res.status(httpCodes.OK).json({msg:'Producto eliminado'});
    }

    static async getProducts(req, res){
        try{
            const products = await Product.findAll({
                where: { status: 1},
                include: [
                    {
                        model: User,
                        attributes: ['id', 'name', 'email'],
                    },
                    {
                        model: Category,
                        attributes: ['id', 'name', 'description']
                    }
                ]
            });
    
            if(!products.length) return res.status(httpCodes.NOT_FOUND).json({msg: 'No existen productos registrados'});
            res.status(httpCodes.OK).json({products});
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
    }

    static async getProductByName(req, res){
        const { name } = req.query;

        try{
            const products = await Product.findAll({
                where: { 
                    status: 1,
                    name: {
                        [Op.like]: `%${name}%`
                    }
                },
                include: [
                    {
                        model: User,
                        attributes: ['id', 'name', 'email'],
                    },
                    {
                        model: Category,
                        attributes: ['id', 'name', 'description']
                    }
                ]
            });
            if(!products.length) return res.status(httpCodes.NOT_FOUND).json({msg: `No existen producto o productos registrados con nombre: ${name}`});
            res.status(httpCodes.OK).json({products});
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
    }

    static async getProductById(req, res){
        const { id } = req.params;

        try{
            const product = await Product.findOne({
                where: { 
                    id, 
                    status: 1 
                },
                include: [
                    {
                        model: User,
                        attributes: ['id', 'name', 'email'],
                    },
                    {
                        model: Category,
                        attributes: ['id', 'name', 'description']
                    }
                ]
            });
            if(!product) return res.status(httpCodes.NOT_FOUND).json({msg: `El producto con ID: ${id}, no existe`});
            res.status(httpCodes.OK).json({product})
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
    }
}

module.exports = ProductController;
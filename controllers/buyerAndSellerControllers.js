const Model = require('../models');
const { User, Transaction, Product,Category } = Model;
const httpCodes = require('../common/httpCodes');

class BuyerAndSellerController{
    static async getBuyers(req, res){
        try{
            const buyerUsers = await Transaction.findAll({ attributes: ['buyer_user']});
            if(!buyerUsers.length) return res.status(httpCodes.NOT_FOUND).json({msg: `No existen usuarios compradores`});
            const users = [];
            buyerUsers.map(x => {
                users.push(x.buyer_user);
            });
            const onlyUsers = [...new Set(users)];

            const getUsers = await User.findAll({ where: { id: onlyUsers } });
            res.status(httpCodes.OK).json({getUsers});
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
    }

    static async getSellers(req, res){
        try{
            const sellerUsers = await Product.findAll({ attributes: ['seller_user']});
            if(!sellerUsers.length) return res.status(httpCodes.NOT_FOUND).json({msg: `No existen usuarios vendedores`});
            const users = [];
            sellerUsers.map(x => {
                users.push(x.seller_user)
            });
            const onlyUsers = [...new Set(users)];
    
            const getUsers = await User.findAll({ where: { id: onlyUsers } });
            res.status(httpCodes.OK).json({getUsers});
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
    }

    static async getCategoriesByBuyer(req, res){
        try{
            const categories = await Product.findAll({ attributes: ['categories']});
            if(!categories.length) return res.status(httpCodes.NOT_FOUND).json({msg: `No existen categorias usadas`});
            const users = [];
            categories.map(x => {
                users.push(x.categories);
            });
            const onlyUsers = [...new Set(users)];
    
            const getCategories = await Category.findAll({ where: { id: onlyUsers } });
            res.status(httpCodes.OK).json({getCategories});
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
    }
}

module.exports = BuyerAndSellerController;
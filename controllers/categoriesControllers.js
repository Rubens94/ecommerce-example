const Model = require('../models');
const { Category } = Model;
const httpCodes = require('../common/httpCodes');
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

class CategoryController {
    static async postCategory(req, res){
        const { name, description } = req.body;

        try{
            const category = await Category.findOne({where: { name }});
            if( category ) return res.status(httpCodes.BAD_REQUEST).json({msg: 'La categoría ya existe, intente con otro nombre.'});
    
            await Category.create({
                name,
                description
            });
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }

        res.status(httpCodes.OK).json({ msg: 'Categoría creada' });
    }

    static async updateCategoryById(req, res){
        const { id } = req.params;
        const { name, description } = req.body;

        try{
            const category = await Category.findOne({where: { id }});
            if(!category) return res.status(httpCodes.NOT_FOUND).json({msg: `La categoría con ID: ${id}, no existe`});

            category.update({
                name,
                description
            });
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }

        res.status(httpCodes.OK).json({msg:'Categoría actualizada'});
    }

    static async deleteCategoryById(req, res){
        const { id } = req.params;

        try{
            const category = await Category.findOne({where: { id }});
            if(!category) return res.status(httpCodes.NOT_FOUND).json({msg: `La categoría con ID: ${id}, no existe`});

            category.destroy();
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }

        res.status(httpCodes.OK).json({msg:'Categoría eliminada'});
    }

    static async getCategories(req, res){
        try{
            const categories = await Category.findAll({});
            if(!categories.length) return res.status(httpCodes.NOT_FOUND).json({msg: 'No existen categorías registradas'});
            res.status(httpCodes.OK).json({categories});
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
    }

    static async getCategoryById(req, res){
        const { id } = req.params;

        try{
            const category = await Category.findOne({where: { id }});
            if(!category) return res.status(httpCodes.NOT_FOUND).json({msg: `La categoría con ID: ${id}, no existe`});
            res.status(httpCodes.OK).json({category})
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
    }

    static async getCategoryByName(req, res){
        const { name } = req.query;

        try{
            const categories = await Category.findAll({
                where: {
                    name: {
                      [Op.like]: `%${name}%`
                    }
                  }
            });
            if(!categories.length) return res.status(httpCodes.NOT_FOUND).json({msg: `No existen categoría o categorías registradas con nombre: ${name}`});
            res.status(httpCodes.OK).json({categories});
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
    }
}

module.exports = CategoryController;
const Model = require('../models');
const { Product, User, Transaction } = Model;
const httpCodes = require('../common/httpCodes');
const jwt = require("jsonwebtoken");

class TransactionController {
    static async postTransaction(req, res){
        const { products, quantity } = req.body;
        const token = req.headers.authorization.split(' ')[1];
        const { id } = jwt.verify(token, process.env.JWT_SECRET);

        if(quantity < 0) return res.status(httpCodes.FORBIDDEN).json({msg: 'No se puede poner una cantidad negativa'});
        try{
            const product = await Product.findOne({ where: {
                id: products,
                status: 1
            }});
            if(!product) return res.status(httpCodes.NOT_FOUND).json({msg: 'Producto no encontrado'});
            if(quantity > product.quantity) return res.status(httpCodes.BAD_REQUEST).json({msg: `Actualmente el producto tiene en stock ${product.quantity} piezas, no se puede realizar la compra`});
            product.quantity -= quantity;
            product.save();


            await Transaction.create({
                buyer_user: id,
                products,
                quantity
            });


        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }

        res.status(httpCodes.OK).json({ msg: 'Transacción realizada' });
    }

    static async updateTransactionById(req, res){
        const { id } = req.params;
        const { quantity } = req.body;

        if(quantity < 0) return res.status(httpCodes.FORBIDDEN).json({msg: 'No se puede poner una cantidad negativa'});
        try{
            const transaction = await Transaction.findOne({where: { id } });
            if(!transaction) res.status(httpCodes.NOT_FOUND).json({msg: `La transacción con ID: ${id}, no existe`});

            const productId = transaction.products;
            const product = await Product.findOne({ where: {id: productId}});
            if(quantity > product.quantity) return res.status(httpCodes.BAD_REQUEST).json({msg: `Actualmente el producto tiene en stock ${product.quantity} piezas, no se puede realizar la compra`});
            if(product.quantity < quantity){
                product.quantity = product.quantity + (transaction.quantity - parseInt(quantity));
                product.save();
            } else {
                product.quantity = product.quantity - ( parseInt(quantity) - transaction.quantity);
                product.save();
            }
    
            transaction.update({
                quantity,
            });
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }

        res.status(httpCodes.OK).json({msg: 'Transacción actualizada'});
    }

    static async deleteTransactionById(req, res){
        const { id } = req.params;

        try{
            const transaction = await Transaction.findOne({where: { id } });
            if(!transaction) res.status(httpCodes.NOT_FOUND).json({msg: `La transacción con ID: ${id}, no existe`});
            const productId = transaction.products;
            const product = await Product.findOne({ where: {id: productId}});

            product.quantity = product.quantity + transaction.products;
            product.save();
    
            transaction.destroy();
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
        
        res.status(httpCodes.OK).json({msg:'Transacción eliminada'});
    }

    static async getTransactionsByAdmin(req, res) {
        try {
            const transactions = await Transaction.findAll({
                include: [
                    {
                        model: User,
                        attributes: ['id', 'name', 'email'],
                    },
                    {
                        model: Product,
                        attributes: ['id', 'name', 'description', 'quantity']
                    }
                ]
            });

            if(!transactions.length) return res.status(httpCodes.NOT_FOUND).json({msg: 'No se encuentran registradas transacciones'});
            res.status(httpCodes.OK).json({transactions});
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
    }

    static async getTransactionsByUser(req, res){
        const token = req.headers.authorization.split(' ')[1];
        const { id } = jwt.verify(token, process.env.JWT_SECRET);
        try{
            const transactions = await Transaction.findAll({
                where: { buyer_user: id},
                include: [
                    {
                        model: User,
                        attributes: ['id', 'name', 'email'],
                    },
                    {
                        model: Product,
                        attributes: ['id', 'name', 'description', 'quantity']
                    }
                ]
            });

            if(!transactions.length) return res.status(httpCodes.NOT_FOUND).json({msg: 'No se encuentran registradas transacciones'});
            res.status(httpCodes.OK).json({transactions});
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
    }

    static async getTransactionsByUserById(req, res){
        const token = req.headers.authorization.split(' ')[1];
        const { id } = jwt.verify(token, process.env.JWT_SECRET);
        const paramId = req.params.id;
        try{
            const transactions = await Transaction.findOne({
                where: { buyer_user: id, id: paramId},
                include: [
                    {
                        model: User,
                        attributes: ['id', 'name', 'email'],
                    },
                    {
                        model: Product,
                        attributes: ['id', 'name', 'description', 'quantity']
                    }
                ]
            });

            if(!transactions) return res.status(httpCodes.NOT_FOUND).json({msg: `No se encuentra registrada la transacción con ID: ${paramId}`});
            res.status(httpCodes.OK).json({transactions});
        } catch (err) {
            res.status(httpCodes.INTERNAL_SERVER_ERROR).json({msg: 'Error en el servidor, contacte al administrador'});
        }
    }
}

module.exports = TransactionController;
const jwt = require('jsonwebtoken');
const models = require('../models');
const { User } = models;
const httpCodes = require('../common/httpCodes');

const isAdminRole = async(req, res, next) => {
    const token = req.headers.authorization.split(' ')[1];
    const { id } = jwt.verify(token, process.env.JWT_SECRET);
    const user = await User.findOne({ where: { id } });
    
    if(user.id !== 1) return res.status(httpCodes.FORBIDDEN).json({msg: 'Acceso denegado'});

    next();
}

module.exports = { isAdminRole };
const jwt = require('jsonwebtoken');
const models = require('../models');
const { User, Transaction } = models;
const httpCodes = require('../common/httpCodes');

const selfOrAdminInTransaction = async(req, res, next) => {
    const token = req.headers.authorization.split(' ')[1];
    const { id } = req.params;

    const jwtDecoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = jwtDecoded.id
    const transactionUser = await Transaction.findOne({ where: { id }});
    const userJwt = await User.findOne({ where: { id: userId }});

    if(!transactionUser) return res.status(httpCodes.NOT_FOUND).json({msg: `Transacción con ID: ${id}, no existe`});
    if(transactionUser.buyer_user !== userId && userJwt.is_admin !== 1) return res.status(httpCodes.FORBIDDEN).json({msg: 'Acceso denegado'});

    next();
}

module.exports = { selfOrAdminInTransaction }
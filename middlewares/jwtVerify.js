const jwt = require('jsonwebtoken');
const models = require('../models');
const { User } = models;
const httpCodes = require('../common/httpCodes');


const jwtVerify = async(req, res, next) => {
    if (!req.headers.authorization) return res.json({ msg: 'no se encuentra el token en la petición' });
    const token = req.headers.authorization.split(' ')[1];

    try {
        const { id } = jwt.verify(token, process.env.JWT_SECRET);
    
        const user = await User.findOne({ where: { id } });
    
        if (!user) return res.status(httpCodes.NOT_FOUND).json({ msg: 'Usuario no encontrado' });
    
        next();
      } catch (err) {
        res.status(httpCodes.BAD_REQUEST).json({ msg: 'Token expirado o no válido' });
      }
}

module.exports = { jwtVerify };
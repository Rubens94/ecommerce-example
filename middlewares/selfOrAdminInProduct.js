const jwt = require('jsonwebtoken');
const models = require('../models');
const { Product, User } = models;
const httpCodes = require('../common/httpCodes');

const selfOrAdminInProduct = async(req, res, next) => {
    const token = req.headers.authorization.split(' ')[1];
    const { id } = req.params;

    const jwtDecoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = jwtDecoded.id
    const product = await Product.findOne({where: { id } });
    const userJwt = await User.findOne({ where: { id: userId }});

    if(!product) return res.status(httpCodes.NOT_FOUND).json({msg: `Producto con ID: ${id}, no existe`});
    if(product.seller_user !== userId && userJwt.is_admin !== 1) return res.status(httpCodes.FORBIDDEN).json({msg: 'Acceso denegado'});

    next();
}

module.exports = { selfOrAdminInProduct }
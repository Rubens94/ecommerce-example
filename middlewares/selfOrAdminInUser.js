const jwt = require('jsonwebtoken');
const models = require('../models');
const { User } = models;
const httpCodes = require('../common/httpCodes');

const selfOrAdminInUser = async(req, res, next) => {
    const token = req.headers.authorization.split(' ')[1];
    const { id } = req.params;

    const jwtDecoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = jwtDecoded.id
    const user = await User.findOne({ where: { id } });
    const userJwt = await User.findOne({ where: { id: userId }});

    if(!user) return res.status(httpCodes.NOT_FOUND).json({msg: `Usuario con ID: ${id}, no existe`});
    if(user.id !== userId && userJwt.is_admin !== 1) return res.status(httpCodes.FORBIDDEN).json({msg: 'Acceso denegado'});

    next();
}

module.exports = { selfOrAdminInUser }
'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Products', [{
      name: "Xiaomi Mi 11T PRO",
      description: "Smartphone de la marca de Xiaomi",
      quantity: 8,
      status: 1,
      seller_user: 2,
      categories: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Products', [{
      name: "Smart tv Samung 55",
      description: "Smart tv de marca Samsung de 55 pulgadas",
      quantity: 4,
      status: 1,
      seller_user: 3,
      categories: 2,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Products', [{
      name: "Tenis Nike 25",
      description: "Tenis negros con blanco Nike del número 25, unisex",
      quantity: 9,
      status: 1,
      seller_user: 3,
      categories: 3,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Products', [{
      name: "Sofa rojo",
      description: "Sofa de piel color rojo",
      quantity: 4,
      status: 1,
      seller_user: 2,
      categories: 4,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Products', null, {});
  }
};

'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Users', [{
      name: "Admin",
      email: "admin@hotmail.com",
      password: "$2b$10$jolvLOzVFBpwoVmMt5zjV.Oqu/B3EbSNGTgMVbywGhJy3a.VuVA2O",
      is_admin: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Users', [{
      name: "Juan Soria",
      email: "juan@hotmail.com",
      password: "$2b$10$tTbHmxbFo4WizluiU5p6UuQQY0JR/0.uVP4TFvIhz.KyrGZhGt0sK",
      is_admin: 0,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Users', [{
      name: "Raúl Gutierrez",
      email: "raul@hotmail.com",
      password: "$2b$10$8vxv8y2kqeilnxZOLVo8juTDNVOdD1Wb1Vwwm3xbQQvw.J58L/7Eu",
      is_admin: 0,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Users', [{
      name: "Sonia Quiroz",
      email: "sonia@hotmail.com",
      password: "$2b$10$FFRR095i0wsSKyIP05LCgOVFfjk3AYju5OoO6Tzh64XV7rfS2DLKO",
      is_admin: 0,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Users', [{
      name: "Juan Miranda",
      email: "juanmiranda@hotmail.com",
      password: "$2b$10$2.PoEX92/V1QkRTbNG.mmOMNZkR6qY3sdsC161DRRo4BgohJW4rAm",
      is_admin: 0,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', null, {});
  }
};

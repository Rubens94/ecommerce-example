# Envíame Challenge

## Instalar dependencias de Nodejs
### npm install
## Ejecutar migraciones
### npx sequelize-cli db:migrate

## Insertar datos de prueba en la BD (seeders)
### npx sequelize-cli db:seed:all

## Ejecutar modo desarrollo
### npm run dev 

## Ejecutar servidor
### npm start

## emails de usuarios de prueba:
### juan@hotmail.com, raul@hotmail.com, sonia@hotmail.com, juanmiranda@hotmail.com. La contraseña de todos es user123

## email de administrador:
### admin@hotmail.com y la contraseña es admin123


## Documentación Postman:
### https://documenter.getpostman.com/view/14715581/UzJEUL5y
________________________________________
## Levantar contenedor de docker
## ejecutar los comandos en el siguiente orden en una terminal:
### docker-compose build
### docker-compose up
## Cuando esten ejecutandose los contenedores, con otra terminal ejecutar las migraciones y seeders:
### docker exec -it enviame bash
###  npx sequelize-cli db:migrate
### npx sequelize-cli db:seed:all
_____________________________________
# Nota:
### el archivo .env en el DB_HOST='mysqldb' apunta al contenedor de docker, si se desea probar la api rest en desarrollo en VS Code cambiar la variable a DB_HOST='localhost'
____________________________________
### En docker al api rest se ejecuta en el puerto 5000 y en desarrollo en el puerto 3000.